class appofyear {
  String _name;
  String _devteam;
  String _year;
  String _sector;
//Constructor
  appofyear(this._name, this._year, this._sector, this._devteam);
//Getters and setters
  String get name => _name;
  set name(String name) {
    _name = name;
  }

  String get year => _year;
  set year(String year) {
    _year = year;
  }

  String get sector => _sector;
  set sector(String sector) {
    _sector = sector;
  }

  String get devteam => _devteam;

  set devteam(String devteam) {
    _devteam = devteam;
  }

  //C in question 3
  Null capsletter() {
    print("Name in capital " + _name.toUpperCase());
    return null;
  }

  String toString() {
    //B in question 3
    print("Name :" + this._name);
    print("Year of Winning: " + this.year);
    print("Sector of Business: " + this._sector);
    print("Team/ Developer(s) : " + this._devteam);
    return super.toString();
  }
}

void main() {
  appofyear fnbapp =
      appofyear("FNB banking app", "2012", "Finance", "FNB dev team");

  fnbapp.toString();
  fnbapp.capsletter();
}
